<%-- 
    Document   : manager_category
    Created on : 19-Oct-2017, 7:30:15 PM
    Author     : tranthanhnhan
--%>

<%@page import="com.tranthanhnhan.shop.model.Category"%>
<%@ page import="com.tranthanhnhan.shop.service.CategoryService" %>
<%@ page import="java.util.List" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý danh mục</title>

        <c:set var="root" value="${pageContext.request.contextPath}"/>
        <link href="${root}/css/mos-style.css" rel='stylesheet' type='text/css' />

    </head>
    <body>

        <%
            CategoryService categoryService = (CategoryService) request.getAttribute("categoryService");
            List<Category> listCategory = categoryService.getAllCategories();
        %>

        <jsp:include page="header.jsp"></jsp:include>

            <div id="wrapper">

            <jsp:include page="menu.jsp"></jsp:include>

                <div id="rightContent">
                    <h3>Quản lý danh mục</h3>
                    
                    <a href="${root}/admin/insert_category.jsp">Thêm danh mục</a>

                    <table class="data">

                        <tr class="data">
                            <th class="data" width="30px">STT</th>
                            <th class="data">Mã danh mục</th>
                            <th class="data">Tên danh mục</th>
                            <th class="data" width="90px">Tùy chọn</th>
                        </tr>

                        <%
                            int count = 0;
                            for(Category category : listCategory){
                                count++;
                        %>
                        <tr class="data">
                            <td class="data" width="30px"><%=count%></td>
                            <td class="data"><%=category.getCategoryID()%></td>
                            <td class="data"><%=category.getCategoryName()%></td>
                            <td class="data" width="90px">
                            <div style="text-align: center;">
                                <a href="${root}/admin/update_category.jsp?command=update&category_id=<%=category.getCategoryID()%>">Sửa</a>&nbsp;&nbsp; | &nbsp;&nbsp;
                                <a href="/admin/delete_category?command=delete&category_id=<%=category.getCategoryID()%>">Xóa</a>
                            </div>
                            </td>
                        </tr>
                        <%}%>

                    </table>
                    <a href="/admin/insert_category">Thêm Danh Mục</a>
                </div>
                <div class="clear"></div>

            <jsp:include page="footer.jsp"></jsp:include>

        </div>

    </body>
</html>
