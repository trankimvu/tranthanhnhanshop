<%@ page import="java.util.Objects" %><%--
    Document   : insert_category
    Created on : 19-Oct-2017, 7:39:12 PM
    Author     : tranthanhnhan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý danh mục</title>

        <c:set var="root" value="${pageContext.request.contextPath}"/>
        <link href="${root}/css/mos-style.css" rel='stylesheet' type='text/css' />
        <style type="text/css">
            .error-message {
                color: red;
            }
        </style>

    </head>
    <body>
        
        <%
            String error = "";
            if(request.getParameter("error")!=null){
                error = (String) request.getParameter("error");
            }

            String command = "insert";
            String categoryId = "";
            if(request.getParameter("category_id")!=null){
                categoryId = (String) request.getParameter("category_id");
                command = "update";
            }
        %>

        <jsp:include page="header.jsp"></jsp:include>

            <div id="wrapper">

            <jsp:include page="menu.jsp"></jsp:include>

                <div id="rightContent">
                    <h3>Thông tin danh mục</h3>
                    <form action="/admin/category" method="post">
                    <table width="95%">
                        <tr>
                            <td style="float: right"><b>Tên danh mục:</b></td>
                            <td><input type="text" class="sedang" name="category_name"><span class="error-message"><%=error%></span></td>
                        </tr>
                        <tr><td></td><td>

                                <input type="hidden" name="command" value="<%=command%>">
                                <%if (Objects.equals("update", command)){%>
                                        <input type="hidden" name="category_id" value="<%=categoryId%>">
                                <%}%>
                                <input type="submit" class="button" value="Lưu dữ liệu">
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
                <div class="clear"></div>

            <jsp:include page="footer.jsp"></jsp:include>

        </div>


    </body>
</html>
