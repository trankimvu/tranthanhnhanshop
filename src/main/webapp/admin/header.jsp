<%-- 
    Document   : header
    Created on : 19-Oct-2017, 7:18:44 PM
    Author     : tranthanhnhan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>header</title>
    </head>
    <body>

        <div id="header">
            <div class="inHeader">
                <div class="mosAdmin">
                    Hallo, Administrator<br>
                    <a href="/">Home Page</a> | <a href="#">Help</a> | <a href="/login">Login</a>
                </div>
                <div class="clear"></div>
            </div>
        </div> 

    </body>
</html>
