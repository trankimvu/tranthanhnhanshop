<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<c:set var="root" value="${pageContext.request.contextPath}"/>
<link href="${root}/css/bootstrap.css" rel="stylesheet">
<link href="${root}/css/style.css" rel="stylesheet">
<link href="${root}/css/etalage.css" rel="stylesheet">
<link href="${root}/css/mos-style.css" rel='stylesheet' type='text/css' />

	<style type="text/css">
		.error-block {
			color: #ff0000;
			background-color: #ffEEEE;
			border: 3px solid #ff0000;
			padding: 8px;
			margin: 16px;
			alignment: left;
		}

		.contact-authority {
			color: red;
		}
	</style>

<title>Tran Thanh Nhan Shop</title>
</head>

<body style="min-width: 1024px; width: auto !important;">

	<div class="container" style="margin-top: 50px">

		<div class="row vertical-align">

			<img alt="Error Icon" src="/images/error-icon.png"
				class="col-xs-2 col-xs-offset-1"
				style="display: inline-block; vertical-align: middle; margin-right: -4px;">


			<div class="col-xs-8">
				<h3>
					Unexpected error occurred
				</h3>

				<div class="error-block">
					<h3>Timestamp: ${timestamp}</h3>
					<h3>Status: ${status}</h3>
					<h3>Error: ${error}</h3>
					<h3>Exception: ${exception}</h3>
					<h3>Message: ${message}</h3>
					<h3>Path: ${path}</h3>
				</div>

				<h3>
					Please <span class="contact-authority">Contact the administrator</span>
				</h3>
				<h3>
					Or go back to <a href="/">Home page</a>
				</h3>
			</div>
		</div>

	</div>

</body>

</html>