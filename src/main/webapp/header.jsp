<%-- 
    Document   : header
    Created on : May 10, 2016, 7:28:30 PM
    Author     : tranthanhnhan
--%>

<%@page import="java.util.Map"%>
<%@page import="com.tranthanhnhan.shop.dto.Item"%>
<%@page import="com.tranthanhnhan.shop.dto.Cart"%>
<%@page import="com.tranthanhnhan.shop.model.User"%>
<%@page import="com.tranthanhnhan.shop.model.Category"%>
<%@ page import="com.tranthanhnhan.shop.service.CategoryService" %>
<%@ page import="org.slf4j.LoggerFactory" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>header</title>

    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=381324158709242";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

</head>
<body>

    <%
        final CategoryService categoryService = (CategoryService) request.getAttribute("categoryService");
        User user = null;
        if (session.getAttribute("user") != null) {
            user = (User) session.getAttribute("user");
        }
        Cart cart = (Cart) session.getAttribute("cart");
        if (cart == null) {
            cart = new Cart();
            session.setAttribute("cart", cart);
        }

    %>

    <!--header-->
    <div class="header">
        <div class="header-top">
            <div class="container">
                <div class="header-top-in">
                    <div class="logo">
                        <a href="index"><img src="images/logo.png" alt=" " ></a>
                    </div>
                    <div class="header-in">
                        <ul class="icon1 sub-icon1">
                            <li><a href="wishlist.html">WISH LIST (0)</a> </li>
                            <li><a href="account.html">  MY ACCOUNT</a></li>
                            <li><a href="#"> SHOPPING CART</a></li>
                            <li><a href="checkout.html" >CHECKOUT</a> </li>
                            <li><div class="cart">
                                    <a href="#" class="cart-in"> </a>
                                    <span> <%=cart.itemCount()%></span>
                                </div>
                                <ul class="sub-icon1 list">
                                    <h3>Recently added items</h3>
                                    <div class="shopping_cart">

                                        <%for (Map.Entry<Long, Item> itemEntry : cart.getCartItems().entrySet()) {%>
                                        <div class="cart_box">
                                            <div class="message">
                                                <div class="alert-close"> </div>
                                                <div class="list_img"><img src="<%=itemEntry.getValue().getProduct().getImage()%>" class="img-responsive" alt=""></div>
                                                <div class="list_desc"><h4><a href="/cart/remove?productID=<%=itemEntry.getValue().getProduct().getId()%>"><%=itemEntry.getValue().getProduct().getName()%></a></h4>
                                                    <%=itemEntry.getValue().getQuantity()%> x<span class="actual"> $<%=itemEntry.getValue().getProduct().getPrice()%></span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <%}%>

                                    </div>
                                    <div class="total">
                                        <div class="total_left">Cart Subtotal: </div>
                                        <div class="total_right">$<%=cart.totalPayable()%></div>
                                        <div class="clearfix"> </div>
                                    </div>
                                    <div class="login_buttons">
                                        <div class="check_button"><a href="/checkout">Check out</a></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </ul>
                            </li>
                            <%if (user != null) {%>
                            <li><a href="/account"><%=user.getEmail()%></a> </li>
                            <li><a href="/logout">Logout</a> </li>
                            <%}%>
                        </ul>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>

        <div class="header-bottom">
            <div class="container">
                <div class="h_menu4">
                    <a class="toggleMenu" href="#">Menu</a>
                    <ul class="nav">
                        <li class="active"><a href="index"><i> </i>Home</a></li>
                        <li><a href="#" >Danh mục</a>
                            <ul class="drop">
                                <%
                                    for (Category c : categoryService.getAllCategories()) {
                                %>
                                <li><a href="product?categoryID=<%=c.getCategoryID()%>&pages=1"><%=c.getCategoryName()%></a></li>
                                    <%
                                        }
                                    %>
                            </ul>
                        </li>
                        <li><a href="products.html" >  Tablets</a></li>
                        <li><a href="products.html" >Components</a></li>
                        <li><a href="products.html" >Software</a></li>
                        <li><a href="products.html" >Phones & PDAs </a></li>
                        <li><a href="products.html" >  Cameras  </a></li>
                        <li><a href="contact" >Contact </a></li>

                    </ul>
                    <script type="text/javascript" src="js/nav.js"></script>
                </div>
            </div>
        </div>


        <div class="header-bottom-in">
            <div class="container">
                <div class="header-bottom-on">
                    <%if (user == null) {%>
                    <p class="wel">Welcome visitor you can <a href="/login">login</a> or <a href="/register">create an account</a></p>
                    <%}%>
                    <div class="header-can">
                        <ul class="social-in">
                            <li><a href="#"><i> </i></a></li>
                            <li><a href="#"><i class="facebook"> </i></a></li>
                            <li><a href="#"><i class="twitter"> </i></a></li>
                            <li><a href="#"><i class="skype"> </i></a></li>
                        </ul>
                        <div class="down-top">
                            <select class="in-drop">
                                <option value="Dollars" class="in-of">Dollars</option>
                                <option value="Euro" class="in-of">Euro</option>
                                <option value="Yen" class="in-of">Yen</option>
                            </select>
                        </div>
                        <div class="search">
                            <form>
                                <input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {
                                                this.value = '';
                                            }" >
                                <input type="submit" value="">
                            </form>
                        </div>

                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

    </div>

</body>
</html>
