package com.tranthanhnhan.shop.controller;

import com.tranthanhnhan.shop.dto.Cart;
import com.tranthanhnhan.shop.dto.Item;
import com.tranthanhnhan.shop.model.Bill;
import com.tranthanhnhan.shop.model.BillDetail;
import com.tranthanhnhan.shop.model.Product;
import com.tranthanhnhan.shop.model.User;
import com.tranthanhnhan.shop.util.SendMail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

@Controller
public class CheckOutController extends AbstractShopController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CheckOutController.class);

    @RequestMapping(path = "/checkout", method = RequestMethod.POST)
    public String remove(HttpSession session, HttpServletRequest request) {
        String payment = request.getParameter("payment");
        String address = request.getParameter("address");
        Cart cart = (Cart) session.getAttribute("cart");
        User user = (User) session.getAttribute("user");

        Bill bill = new Bill();
        bill.setAddress(address);
        bill.setPayment(payment);
        bill.setUser(user);
        bill.setDate(new Timestamp(new Date().getTime()));
        bill.setTotal(cart.totalPayable());
        billService.addBill(bill);

        for (Map.Entry<Long, Item> cartEntry : cart.getCartItems().entrySet()) {
            BillDetail billDetail = new BillDetail();
            billDetail.setProduct(cartEntry.getValue().getProduct());
            billDetail.setPrice(cartEntry.getValue().getProduct().getPrice());
            billDetail.setQuantity(cartEntry.getValue().getQuantity());
            bill.addBillDetail(billDetail);
            billDetail = billService.addBillDetail(billDetail);
        }

        //SendMail.sendMail(user.getEmail(), "Tran Thanh Nhan Shop Don Hang", "Hello, "+user.getEmail()+"\nTotal: "+cart.totalPayable());
        cart = new Cart();
        session.setAttribute("cart", cart);

        return getPreviousPageByRequest(request).orElse(REDIRECT + "/");
    }

}
