package com.tranthanhnhan.shop.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ShopApplicationWebAdminController extends AbstractShopController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShopApplicationWebAdminController.class);

    @RequestMapping({"/admin/", "/admin/index", "/admin/index.jsp"})
    public ModelAndView index() {
        return modelAndViewWithAllServices("admin/index");
    }


    @RequestMapping({"/admin/login", "/admin/login.jsp"})
    public ModelAndView login() {
        return modelAndViewWithAllServices("admin/login");
    }

    @RequestMapping({"/admin/manager_category", "/admin/manager_category.jsp"})
    public ModelAndView managerCategory() {
        return modelAndViewWithAllServices("admin/manager_category");
    }
    @RequestMapping({"/admin/insert_category", "/admin/insert_category.jsp"})
    public ModelAndView insertCategory() {
        return modelAndViewWithAllServices("admin/insert_category");
    }

    @RequestMapping({"/admin/update_category", "/admin/update_category.jsp"})
    public ModelAndView update_category() {
        return modelAndViewWithAllServices("admin/update_category");
    }

    @RequestMapping({"/admin/manager_product", "/admin/manager_product.jsp"})
    public ModelAndView managerProduct() {
        return modelAndViewWithAllServices("admin/manager_product");
    }

    @RequestMapping({"/admin/insert_product", "/admin/insert_product.jsp"})
    public ModelAndView insertProduct() {
        return modelAndViewWithAllServices("admin/insert_product");
    }

    @RequestMapping({"/admin/manager_bill", "/admin/manager_bill.jsp"})
    public ModelAndView managerBill() {
        return modelAndViewWithAllServices("admin/manager_bill");
    }

    @RequestMapping({"/admin/manager_chart", "/admin/manager_chart.jsp"})
    public ModelAndView managerChart() {
        return modelAndViewWithAllServices("admin/manager_chart");
    }

}
