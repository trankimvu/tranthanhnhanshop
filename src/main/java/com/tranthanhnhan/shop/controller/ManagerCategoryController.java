package com.tranthanhnhan.shop.controller;

import com.tranthanhnhan.shop.dao.CategoryDAO;
import com.tranthanhnhan.shop.model.Category;
import com.tranthanhnhan.shop.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Map;
import java.util.Objects;

@Controller
public class ManagerCategoryController extends AbstractShopController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManagerCategoryController.class);

    @RequestMapping(path = "/admin/delete_category", method = RequestMethod.GET)
    public String deleteCategory(@RequestParam Map<String,String> requestParams) {
        final String command = requestParams.get("command");
        final String categoryId = requestParams.get("category_id");

        if (Objects.equals("delete", command) && categoryId != null){
            categoryService.deleteCategory(Long.valueOf(categoryId));
        }
        return REDIRECT +"/admin/manager_category";
    }

    @RequestMapping(path = "/admin/category", method = RequestMethod.POST)
    public String category(@RequestParam Map<String,String> requestParams, RedirectAttributes redirectAttributes) {
        final String command = requestParams.get("command");
        final String categoryId = requestParams.get("category_id");

        if (StringUtils.isEmpty(command)){
            return REDIRECT +"/admin/manager_category";
        }

        final String categoryName = requestParams.get("category_name");

        String url = "/admin/manager_category";
        String errorMesage = null;
        if (StringUtils.isEmpty(categoryName)) {
            errorMesage = "Vui lòng nhập tên danh mục!";
            redirectAttributes.addAttribute("error", errorMesage);
            redirectAttributes.addAttribute("category_id", categoryId);
        }

        if (StringUtils.isEmpty(errorMesage)){
            switch (command){
                case "insert":
                    categoryService.addCategory(new Category(categoryName));
                    break;
                case "update":
                    if (StringUtils.isEmpty(categoryId)){
                        LOGGER.error("Trying to update a Category with no Id info. Request params: {}", requestParams);
                        return REDIRECT +"/admin/manager_category";
                    }
                    categoryService.updateCategory(Long.valueOf(categoryId), categoryName);
                    break;
            }
        } else {
            url = "/admin/insert_category";
        }

        return REDIRECT +url;
    }

}
