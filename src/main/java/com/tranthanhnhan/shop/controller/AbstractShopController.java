package com.tranthanhnhan.shop.controller;

import com.tranthanhnhan.shop.service.BillService;
import com.tranthanhnhan.shop.service.CategoryService;
import com.tranthanhnhan.shop.service.ChartService;
import com.tranthanhnhan.shop.service.ProductService;
import com.tranthanhnhan.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

abstract class AbstractShopController {
    protected static final String REDIRECT = "redirect:";
    @Autowired
    protected CategoryService categoryService;
    @Autowired
    protected ProductService productService;
    @Autowired
    protected BillService billService;
    @Autowired
    protected UserService userService;
    @Autowired
    protected ChartService chartService;

    protected ModelAndView modelAndViewWithAllServices(String viewName){
        final ModelAndView modelAndView = new ModelAndView()
                .addObject("categoryService", categoryService)
                .addObject("productService", productService)
                .addObject("billService", billService)
                .addObject("userService", userService)
                .addObject("chartService", chartService);
        modelAndView.setViewName(viewName);
        return modelAndView;
    }

    /**
     * Returns the viewName to return for coming back to the sender url
     *
     * @param request Instance of {@link HttpServletRequest} or use an injected instance
     * @return Optional with the view name. Recommended to use an alternative url with
     * {@link Optional#orElse(java.lang.Object)}
     */
    protected Optional<String> getPreviousPageByRequest(HttpServletRequest request)
    {
        return Optional.ofNullable(request.getHeader("Referer")).map(requestUrl -> "redirect:" + requestUrl);
    }
}
