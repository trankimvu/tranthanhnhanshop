package com.tranthanhnhan.shop.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ShopApplicationWebController extends AbstractShopController{

    private static final Logger LOGGER = LoggerFactory.getLogger(ShopApplicationWebController.class);

    @RequestMapping({"/", "/index", "/index.jsp"})
    public ModelAndView index() {
        LOGGER.info("LOG00000: Requesting with url: /");
        return modelAndViewWithAllServices("index");

    }

    @RequestMapping({"/product", "/product.jsp"})
    public ModelAndView product() {
        LOGGER.info("LOG00001: Requesting with url: /product");
        return modelAndViewWithAllServices("product");
    }

    @RequestMapping({"/single", "/single.jsp"})
    public ModelAndView single() {
        LOGGER.info("LOG00002: Requesting with url: /single");
        return modelAndViewWithAllServices("single");
    }

    @RequestMapping({"/contact", "/contact.jsp"})
    public ModelAndView contact() {
        LOGGER.info("LOG00003: Requesting with url: /contact");
        return modelAndViewWithAllServices("contact");
    }

    @RequestMapping({"/register", "/register.jsp"})
    public ModelAndView register() {
        LOGGER.info("LOG00004: Requesting with url: /register");
        return modelAndViewWithAllServices("register");
    }

    @RequestMapping({"/login", "/login.jsp"})
    public ModelAndView login() {
        LOGGER.info("LOG00005: Requesting with url: /login");
        return modelAndViewWithAllServices("login");
    }

    @RequestMapping({"/checkout", "/checkout.jsp"})
    public ModelAndView checkout() {
        LOGGER.info("LOG00006: Requesting with url: /checkout");
        return modelAndViewWithAllServices("checkout");
    }

}
