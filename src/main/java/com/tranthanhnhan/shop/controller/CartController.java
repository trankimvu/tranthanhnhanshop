package com.tranthanhnhan.shop.controller;

import com.tranthanhnhan.shop.dto.Cart;
import com.tranthanhnhan.shop.dto.Item;
import com.tranthanhnhan.shop.model.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

@Controller
public class CartController extends AbstractShopController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CartController.class);

    @RequestMapping(path = "/cart/insert")
    public String insert(@RequestParam Map<String, String> requestParams, HttpSession session, HttpServletRequest request) {

        Cart cart = (Cart) session.getAttribute("cart");
        if (cart == null){
            cart = new Cart();
            session.setAttribute("cart", cart);
        }

        Long idProduct = Long.parseLong(requestParams.get("productID"));
        Product product = productService.getProductById(idProduct);

        if (cart.getCartItems().containsKey(idProduct)) {
            cart.plusToCart(idProduct, new Item(product,
                    cart.getCartItems().get(idProduct).getQuantity()));
        } else {
            cart.plusToCart(idProduct, new Item(product, 1));
        }

        return getPreviousPageByRequest(request).orElse(REDIRECT + "/");

    }

    @RequestMapping(path = "/cart/remove")
    public String remove(@RequestParam Map<String, String> requestParams, HttpSession session, HttpServletRequest request) {
        Cart cart = (Cart) session.getAttribute("cart");

        cart.removeFromCart(Long.parseLong(requestParams.get("productID")));
        return getPreviousPageByRequest(request).orElse(REDIRECT + "/");
    }
}
