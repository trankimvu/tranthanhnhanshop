package com.tranthanhnhan.shop.controller;

import com.tranthanhnhan.shop.model.User;
import com.tranthanhnhan.shop.service.UserService;
import com.tranthanhnhan.shop.util.MD5;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
public class UserController extends AbstractShopController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @RequestMapping(path = "/user", method = RequestMethod.POST)
    public String user(@RequestParam Map<String,String> requestParams, RedirectAttributes redirectAttributes, HttpSession session, HttpServletRequest request) {
        switch (requestParams.get("command")) {
            case "insert":
                User newUser;
                newUser = new User();
                newUser.setEmail(requestParams.get("email"));
                newUser.setPassword(MD5.encryption(requestParams.get("pass")));
                newUser.setAdmin(false);
                newUser = userService.addUser(newUser);
                session.setAttribute("user", newUser);
                return REDIRECT+ "/";
            case "login":
                String previousPage = requestParams.get("previousPage");
                User user = userService.login(requestParams.get("email"), MD5.encryption(requestParams.get("pass")));
                if (user != null) {
                    session.setAttribute("user", user);
                    if (!StringUtils.isEmpty(previousPage)){
                        return REDIRECT+ previousPage;
                    } else {
                        return getPreviousPageByRequest(request).orElse(REDIRECT+ "/");
                    }

                } else {
                    redirectAttributes.addAttribute("error", "Wrong username or password!");
                    redirectAttributes.addAttribute("previousPage", getPreviousPageByRequest(request).orElse(null));
                    return REDIRECT+ "/login";
                }
            default:
                LOGGER.warn("Command received is not supported, redirecting to login page. Request params: {}", requestParams);
                return REDIRECT+ "/login";
        }
    }

    @ResponseBody
    @RequestMapping(path = "/user/isUserNameAvailable", method = RequestMethod.POST)
    public boolean userNameIsAvaible(@RequestParam Map<String,String> requestParams){
        String username = requestParams.get("username");
        if (StringUtils.isEmpty(username)){
            return false;
        }
        return userService.isUserNameAvailable(username);
    }

    @RequestMapping(path = "/logout")
    public String logout(HttpSession session) {
        session.removeAttribute("user");
        return REDIRECT+ "/login";
    }

}
