
package com.tranthanhnhan.shop.controller;

import com.tranthanhnhan.shop.dao.BillDAO;
import com.tranthanhnhan.shop.dao.BillDetailDAO;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.tranthanhnhan.shop.model.Bill;
import com.tranthanhnhan.shop.model.BillDetail;
import com.tranthanhnhan.shop.dto.Cart;
import com.tranthanhnhan.shop.dto.Item;
import com.tranthanhnhan.shop.model.User;
import com.tranthanhnhan.shop.util.SendMail;

/**
 *
 * @author tranthanhnhan
 */
public class CheckOutServlet extends HttpServlet {

    private final BillDAO billDAO = new BillDAO();
    private final BillDetailDAO billDetailDAO = new BillDetailDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String payment = request.getParameter("payment");
        String address = request.getParameter("address");
        HttpSession session = request.getSession();
        Cart cart = (Cart) session.getAttribute("cart");
        User users = (User) session.getAttribute("user");
        try {
            long ID = new Date().getTime();
            Bill bill = new Bill();
            bill.setId(ID);
            bill.setAddress(address);
            bill.setPayment(payment);
//            bill.setUserID(users.getId());
            bill.setDate(new Timestamp(new Date().getTime()));
            bill.setTotal(cart.totalPayable());
            billDAO.insertBill(bill);
            for (Map.Entry<Long, Item> list : cart.getCartItems().entrySet()) {
                billDetailDAO.insertBillDetail(new BillDetail(0, ID,
                        list.getValue().getProduct().getProductID(),
                        list.getValue().getProduct().getPrice(),
                        list.getValue().getQuantity()));
            }
            SendMail sm = new SendMail();
            sm.sendMail(users.getEmail(), "Kenh Lap Trinh", "Hello, "+users.getEmail()+"\nTotal: "+cart.totalPayable());
            cart = new Cart();
            session.setAttribute("cart", cart);
        } catch (Exception e) {
        }
        response.sendRedirect("/shop/index.jsp");
    }

}
