package com.tranthanhnhan.shop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = ShopApplicationConfiguration.class)
public class ShopApplicationConfiguration extends WebMvcConfigurerAdapter implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShopApplicationConfiguration.class);
    @Autowired
    private InternalResourceViewResolver internalResourceViewResolver;


    @Override
    public void afterPropertiesSet() throws Exception {
        internalResourceViewResolver.setExposeContextBeansAsAttributes(true);
        LOGGER.info("Context beans exposed as attributes. Beans are now accessible in plain ${...} in a JSP 2.0 page, as well as in JSTL's c:out value expressions...");
    }
}
