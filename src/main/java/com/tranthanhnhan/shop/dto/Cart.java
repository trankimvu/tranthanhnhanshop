
package com.tranthanhnhan.shop.dto;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author tranthanhnhan
 */
public class Cart {

    private Map<Long, Item> cartItems = new HashMap<>();

    public Cart() {}

    public Map<Long, Item> getCartItems() {
        return cartItems;
    }

    // insert to cart
    public void plusToCart(Long key, Item item) {
        if (cartItems.containsKey(key)) {
            int quantity_old = item.getQuantity();
            item.setQuantity(quantity_old + 1);
            cartItems.put(key, item);
        } else {
            cartItems.put(key, item);
        }
    }

    public void removeFromCart(Long key) {
        cartItems.remove(key);
    }

    public int itemCount() {
        return cartItems.size();
    }

    // sum total
    public double totalPayable() {
        double count = 0;
        // count = price * quantity
        for (Map.Entry<Long, Item> list : cartItems.entrySet()) {
            count += list.getValue().getProduct().getPrice() * list.getValue().getQuantity();
        }
        return count;
    }

}
