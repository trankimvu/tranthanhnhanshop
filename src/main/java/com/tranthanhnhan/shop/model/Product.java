
package com.tranthanhnhan.shop.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author tranthanhnhan
 */
@Entity
@Table(name = "PRODUCT")
@AttributeOverride(name = "id", column = @Column(name = "PRODUCT_ID"))
public class Product extends AbstractEntity{

    @NotNull
    @JoinColumn(name = "CATEGORY_ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private Category category;

    @Column(name = "PRODUCT_NAME")
    private String name;

    @Column(name = "PRODUCT_IMAGE")
    private String image;

    @Column(name = "PRODUCT_PRICE")
    private double price;

    @Column(name = "PRODUCT_DESCRIPTION")
    private String description;

    public Product() {}

    public long getProductID() {
        return id;
    }

    public void setProductID(long productID) {
        this.id = productID;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
