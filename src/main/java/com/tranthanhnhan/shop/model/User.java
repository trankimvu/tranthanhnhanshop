
package com.tranthanhnhan.shop.model;

import org.hibernate.annotations.Type;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author tranthanhnhan
 */
@Entity
@Table(name = "USERS")
@AttributeOverride(name = "id", column = @Column(name = "USER_ID"))
public class User extends AbstractEntity{

    @Column(name = "USER_EMAIL")
    private String email;

    @Column(name = "USER_PASS")
    private String password;

    @Type(type = "numeric_boolean")
    @Column(name = "USER_ROLE")
    private boolean admin;

    public User() {}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

}
