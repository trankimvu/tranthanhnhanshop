
package com.tranthanhnhan.shop.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author tranthanhnhan
 */
@Entity
@Table(name = "BILL")
@AttributeOverride(name = "id", column = @Column(name = "BILL_ID"))
public class Bill extends AbstractEntity{

    @JoinColumn(name = "USER_ID")
    @OneToOne
    private User user;

    @Column(name = "TOTAL")
    private double total;

    @Column(name = "PAYMENT")
    private String payment;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "DATE")
    private Timestamp date;

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "bill")
    private Set<BillDetail> billDetails = new HashSet<>();

    public Bill() {}

    public Bill(long billID, long userID, double total, String payment, String address, Timestamp date) {
        this.total = total;
        this.payment = payment;
        this.address = address;
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Set<BillDetail> getBillDetails() {
        return Collections.unmodifiableSet(billDetails);
    }

    public void addBillDetail(BillDetail billDetail){
        billDetail.setBill(this);
        billDetails.add(billDetail);
    }
}
