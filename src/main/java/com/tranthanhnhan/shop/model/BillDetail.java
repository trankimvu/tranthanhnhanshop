
package com.tranthanhnhan.shop.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author tranthanhnhan
 */
@Entity
@Table(name = "BILL_DETAIL")
@AttributeOverride(name = "id", column = @Column(name = "BILL_DETAIL_ID"))
public class BillDetail extends AbstractEntity{

    @NotNull
    @JoinColumn(name = "BILL_ID")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Bill bill;

    @NotNull
    @JoinColumn(name = "PRODUCT_ID")
    @OneToOne
    private Product product;

    @Column(name = "PRICE")
    private double price;

    @Column(name = "QUANTITY")
    private int quantity;

    public BillDetail() {}

    public BillDetail(long billDetailID, long billID, long productID, double price, int quantity) {
        this.id = billDetailID;
        this.price = price;
        this.quantity = quantity;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public long getBillDetailID() {
        return id;
    }

    public void setBillDetailID(long billDetailID) {
        this.id = billDetailID;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
