package com.tranthanhnhan.shop.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @param <I> type of entity ID
 */
@MappedSuperclass
abstract class BaseEntity<I extends Serializable> {

    @Size(max = 20)
    @Column(name = "USR_LOG_I", updatable = false)
    private String usrLogI;

    @Column(name = "DTE_LOG_I", updatable = false)
    private LocalDateTime dteLogI;

    @Size(max = 20)
    @Column(name = "USR_LOG_U")
    private String usrLogU;

    @Column(name = "DTE_LOG_U")
    private LocalDateTime dteLogU;

    @Version
    @Column(name = "VERSION")
    private Integer version;

    @Transient
    private boolean transientHashCodeLeaked = false;

    @Transient
    private String rawClassName = getClass().getName();

    @PrePersist
    public void beforeSave() {
        setUsrLogI("script");
        setDteLogI(LocalDateTime.now());
        setUsrLogU("script");
        setDteLogU(LocalDateTime.now());
    }

	@PreUpdate
	public void beforeUpdate() {
		setUsrLogU("script");
        setDteLogU(LocalDateTime.now());
    }

    public abstract I getId();

    public void setUsrLogI(String usrLogI) {
        this.usrLogI = usrLogI;
    }

    public void setDteLogI(LocalDateTime dteLogI) {
        this.dteLogI = dteLogI;
    }

	public void setUsrLogU(String usrLogU) {
		this.usrLogU = usrLogU;
	}

    public void setDteLogU(LocalDateTime dteLogU) {
        this.dteLogU = dteLogU;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public boolean isPersisted() {
        return getId() != null;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        /*
         * The following is a solution that works for hibernate lazy loading proxies. if (getClass() !=
         * HibernateProxyHelper.getClassWithoutInitializingProxy(obj)) { return false; }
         */
        if (obj instanceof BaseEntity) {
            final BaseEntity other = (BaseEntity) obj;
            if (isPersisted() && other.isPersisted()) { // both entities are not new

                // Because entities are currently used in clientside, we cannot use HibernateProxyHelper here >
                // we cannot compare class for sure they are the same class, just compare ID.
                return getId().equals(other.getId()) && rawClassName.equals(other.rawClassName);
            }
            // if one of entity is new (transient), they are considered not equal.
        }
        return false;
    }

    @Override
    public int hashCode() {
        if (!isPersisted()) { // is new or is in transient state.
            transientHashCodeLeaked = true;
            return -super.hashCode();
        }

        // because hashcode has just been asked for when the object is in transient state
        // at that time, super.hashCode(); is returned. Now for consistency, we return the
        // same value.
        if (transientHashCodeLeaked) {
            return -super.hashCode();
        }
        return getId().hashCode();
        // The above mechanism obey the rule: if 2 objects are equal, their hashcode must be same.
    }
}
