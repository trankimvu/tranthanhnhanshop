
package com.tranthanhnhan.shop.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author tranthanhnhan
 */
public class SendMail {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendMail.class);

    public static boolean sendMail(String to, String subject, String text) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("tranthanhnhanshop@gmail.com", "123456789");
            }
        });
        try {
            Message message = new MimeMessage(session);
            message.setHeader("Content-Type", "text/plain; charset=UTF-8");
            message.setFrom(new InternetAddress("tranthanhnhanshop@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(subject);
            message.setText(text);
            Transport.send(message);
        } catch (MessagingException e) {
            LOGGER.error("LOG00020: Failed to send email with Subject: {}\nTo: {}\nContent: {}", subject, to, text, e);
            return false;
        }
        return true;
    }
    
    public static void main(String[] args) {
        System.out.println(sendMail("vu.tran04@gmail.com", "TRANTHANHNHANSHOP don hang", "Hello, Vu"));
    }

}
