package com.tranthanhnhan.shop.repository;

import com.tranthanhnhan.shop.model.Category;
import com.tranthanhnhan.shop.model.Product;
import com.tranthanhnhan.shop.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findProductsByCategory(Category category);

    @Query(nativeQuery = true, value = "SELECT * FROM PRODUCT WHERE category_id = :categoryId LIMIT :firstResult,:maxResult")
    List<Product> findByCategoryIdLimitted(@Param("categoryId") Long categoryId,@Param("firstResult") Long firstResult,@Param("maxResult") Long maxResult);
}
