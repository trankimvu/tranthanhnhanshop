package com.tranthanhnhan.shop.repository;

import com.tranthanhnhan.shop.model.Bill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BillRepository extends JpaRepository<Bill, Long> {
}
