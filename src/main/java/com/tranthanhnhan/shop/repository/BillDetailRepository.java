package com.tranthanhnhan.shop.repository;

import com.tranthanhnhan.shop.model.BillDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BillDetailRepository extends JpaRepository<BillDetail, Long> {
}
