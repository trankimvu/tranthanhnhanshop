package com.tranthanhnhan.shop.service;

import com.tranthanhnhan.shop.model.User;

public interface UserService {
    User addUser(User user);
    User getUser(Long userId);
    boolean isUserNameAvailable(String email);
    User login(String email, String password);
}
