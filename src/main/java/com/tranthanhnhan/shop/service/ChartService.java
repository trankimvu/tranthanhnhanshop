package com.tranthanhnhan.shop.service;

import com.tranthanhnhan.shop.dto.Value;

import java.util.List;

public interface ChartService {
    List<Value> getSummary();
}
