package com.tranthanhnhan.shop.service;

import com.tranthanhnhan.shop.model.Bill;
import com.tranthanhnhan.shop.model.BillDetail;

import java.util.List;

public interface BillService {
    List<Bill> getAllBills();
    Bill addBill(Bill bill);
    BillDetail addBillDetail(BillDetail billDetail);
}
