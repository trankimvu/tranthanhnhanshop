package com.tranthanhnhan.shop.service;

import com.tranthanhnhan.shop.model.Product;

import java.util.List;

public interface ProductService {

    Product getProductById(Long productId);

    Long countProductByCategory(Long categoryId);

    List<Product> getProductByCategory(Long categoryId);

    List<Product> getListProductByNav(long categoryId, long firstResult, long maxResult);

}
