package com.tranthanhnhan.shop.service;

import com.tranthanhnhan.shop.model.Category;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategories();
    Category addCategory(Category category);
    Category updateCategory(Category category);
    void deleteCategory(Long categoryId);
    void updateCategory(Long categoryId, String categoryName);
}
