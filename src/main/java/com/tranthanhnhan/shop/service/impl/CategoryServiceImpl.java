package com.tranthanhnhan.shop.service.impl;

import com.tranthanhnhan.shop.model.Category;
import com.tranthanhnhan.shop.repository.CategoryRepository;
import com.tranthanhnhan.shop.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public Category addCategory(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public Category updateCategory(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public void deleteCategory(Long categoryId) {
        categoryRepository.delete(categoryId);
    }

    @Override
    public void updateCategory(Long categoryId, String categoryName) {
        final Category category = categoryRepository.findOne(categoryId);
        if (category != null){
            category.setCategoryName(categoryName);
            categoryRepository.save(category);
        }
    }
}
