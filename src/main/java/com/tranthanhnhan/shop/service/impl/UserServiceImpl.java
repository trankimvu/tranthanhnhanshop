package com.tranthanhnhan.shop.service.impl;

import com.tranthanhnhan.shop.model.User;
import com.tranthanhnhan.shop.repository.UserRepository;
import com.tranthanhnhan.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User addUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User getUser(Long userId) {
        return userRepository.findOne(userId);
    }

    //TODO, write unit test for this method
    @Override
    public boolean isUserNameAvailable(String email) {
        return userRepository.findUserByEmail(email) == null;
    }

    //TODO, write unit test for this method
    @Override
    public User login(String email, String password) {
        return userRepository.findUserByEmailAndPassword(email, password);
    }
}
