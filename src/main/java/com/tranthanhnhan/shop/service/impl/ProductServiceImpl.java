package com.tranthanhnhan.shop.service.impl;

import com.tranthanhnhan.shop.model.Product;
import com.tranthanhnhan.shop.repository.CategoryRepository;
import com.tranthanhnhan.shop.repository.ProductRepository;
import com.tranthanhnhan.shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;


    @Override
    public Product getProductById(Long productId) {
        return productRepository.findOne(productId);
    }

    //TODO Write unit test for this method
    @Override
    public Long countProductByCategory(Long categoryId) {
        return (long) getProductByCategory(categoryId).size();
    }

    @Override
    public List<Product> getProductByCategory(Long categoryId) {
        return productRepository.findProductsByCategory(categoryRepository.findOne(categoryId));
    }

    //TODO Write unit test for this method
    @Override
    public List<Product> getListProductByNav(long categoryId, long firstResult, long maxResult) {
        return productRepository.findByCategoryIdLimitted(categoryId, firstResult, maxResult);
    }
}
