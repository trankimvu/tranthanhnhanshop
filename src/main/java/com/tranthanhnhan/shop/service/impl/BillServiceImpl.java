package com.tranthanhnhan.shop.service.impl;

import com.tranthanhnhan.shop.model.Bill;
import com.tranthanhnhan.shop.model.BillDetail;
import com.tranthanhnhan.shop.repository.BillDetailRepository;
import com.tranthanhnhan.shop.repository.BillRepository;
import com.tranthanhnhan.shop.service.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BillServiceImpl implements BillService {

    @Autowired
    private BillRepository billRepository;

    @Autowired
    private BillDetailRepository billDetailRepository;

    @Override
    public List<Bill> getAllBills() {
        return billRepository.findAll();
    }

    @Override
    public Bill addBill(Bill bill) {
        return billRepository.save(bill);
    }

    @Override
    public BillDetail addBillDetail(BillDetail billDetail) {
        return billDetailRepository.save(billDetail);
    }
}
