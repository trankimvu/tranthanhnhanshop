package com.tranthanhnhan.shop.service.impl;

import com.tranthanhnhan.shop.dto.Value;
import com.tranthanhnhan.shop.model.Category;
import com.tranthanhnhan.shop.repository.CategoryRepository;
import com.tranthanhnhan.shop.service.ChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChartServiceImpl implements ChartService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Value> getSummary() {
        return categoryRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(Category::getCategoryName))
                .map(category -> new Value(category.getCategoryName(), category.getProducts().size()))
                .collect(Collectors.toList());
    }
}
